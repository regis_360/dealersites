const mix = require('laravel-mix');

/*
 |--------------------------------------------------------------------------
 | Mix Asset Management
 |--------------------------------------------------------------------------
 |
 | Mix provides a clean, fluent API for defining some Webpack build steps
 | for your Laravel application. By default, we are compiling the Sass
 | file for the application as well as bundling up all the JS files.
 |
 */

mix.sass('resources/sass/app.scss','public/css/app.css')
    .js('node_modules/cpf-cnpj-validator/dist/cpf-cnpj-validator.cjs.js','public/js/cpf-cnpj-validator.js')
    .js('node_modules/bootstrap/dist/js/bootstrap.bundle.js','public/js/bootstrap.js');
