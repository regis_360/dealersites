<?php

namespace App\Http\Controllers;

use App\Loja;
use App\Menu;
use App\Telefone;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class HomeController extends Controller
{
    //
    public function index()
    {
        $menus = Menu::all()->take(10);
        $lojas = DB::table('lojas')
            ->join('loja_enderecos','lojas.id','loja_enderecos.loja_id')
            ->get();
        $telefones = Telefone::all();

        $carsModel = [178,179,180,182,185,190,440,473];
        return view('home/index',
            ['menus'=>$menus, 'lojas'=>$lojas, 'telefones'=>$telefones, 'carsModel'=>$carsModel]
        );
    }

    public function contact(Request $request)
    {
        return response()->json(['message' => 'Mensagem enviada com sucesso!']);
    }
}
