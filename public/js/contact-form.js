$().ready( function () {
    $('#contact-form').validate({
        errorClass: "alert alert-danger mt-1 w-100",
        errorElement: 'div',
        rules: {
            name: {
                required: true,
                minlength: 2
            },
            phone: {
                required: true,
                minlength: 10,
                maxlength: 11,
            },
            email:{
                required: true,
                email: true
            },
            cpfCnpj:{
                required: true
            }
        },
        submitHandler:
            $(function() {
                $('form[name="contact-form"]').submit(function (e) {
                    e.preventDefault();
                    $.ajax({
                        url: "{{ route('contact') }}",
                        method: "POST",
                        data: $(this).serialize(),
                        dataType: 'json',
                        success: function (response) {
                            console.log(response);
                        }
                    });
                });
            })
    });

});
