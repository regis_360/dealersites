$(document).ready(function () {
    $(".loja-header").on("click", function () {
        var lojaSelcted = $(this).attr('data-loja');
        $(".loja-header-option").each(function () {
            if($(this).attr('data-loja') == lojaSelcted){
                $(this).removeClass('d-none');
            }else{
                $(this).addClass('d-none');
            }
        })
    });

    $("#lojaInfoSelect").on("change", function () {
        var lojaSelcted = $(this).val();
        console.log(lojaSelcted);
        $(".lojaInfoDesc").each(function () {
            if($(this).attr('data-loja') == lojaSelcted){
                $(this).removeClass('d-none');
            }else{
                $(this).addClass('d-none');
            }
        })
    });

    var cpfCNPJMaskBehavior = function (val) {
            return val.replace(/\D/g, '').length >= 12 ? '00.000.000/0000-00' : '000.000.000-0099';
        },
        cpfCNPJOptions = {
            onKeyPress: function(val, e, field, options) {
                field.mask(cpfCNPJMaskBehavior.apply({}, arguments), options);
            }
        };

    $('.cpfCNPJ').mask(cpfCNPJMaskBehavior,  cpfCNPJOptions);

    var PhoneMaskBehavior = function (val) {
            return val.replace(/\D/g, '').length === 11 ? '(00) 00000-0000' : '(00) 0000-00009';
        },
        PhoneOptions = {
            onKeyPress: function(val, e, field, options) {
                field.mask(PhoneMaskBehavior.apply({}, arguments), options);
            }
        };

    $('.phones').mask(PhoneMaskBehavior, PhoneOptions);
})
