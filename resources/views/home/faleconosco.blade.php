<div class="bg-gray text-primary p-5">
    <div class="text-center">
        <h3>Fale conosco</h3>
        <p>Para mais informações, por favor, preencha o formulário abaixo que entraremos em contato rapidamente.</p>
    </div>
    <form action="/contact" method="POST" id="contact-form" name="contact-form">
        @csrf
        <div class="d-flex flex-wrap">
            <div class="col-12 col-lg-6">
                <div class="form-group">
                    <label for="lojaSelect">Em qual unidade deseja ser atendido?</label>
                    <select id="lojaSelect" name="loja" class="custom-select">
                        @foreach($lojas as $loja)
                            <option value="{{ $loja->id }}" {{($lojas[0] == $loja)?"selected":""}}>{{ $loja->name }}</option>
                        @endforeach
                    </select>
                </div>
                <div class="form-group">
                    <input class="form-control" type="text" id="name" name="name" placeholder="Nome completo">
                </div>
                <div class="form-group">
                    <input class="form-control phones" type="text" id="phone" name="phone" placeholder="Telefone">
                </div>
                <div class="form-group">
                    <input class="form-control" type="text" id="email" name="email" placeholder="Email">
                </div>
            </div>
            <div class="col-12 col-lg-6">
                <div class="form-group">
                    <label for="doubts">Alguma dúvida ou observação? Escreva aqui.</label>
                    <textarea id="doubts" class="form-control" name="doubts" rows="3" placeholder="Alguma dúvida ou observação? Escreva aqui."></textarea>
                </div>
                <div class="form-group">
                    <label for="cpfCnpj">CPF ou CNPJ</label>
                    <input id="cpfCnpj" class="form-control  cpfCNPJ" type="text" name="cpfCnpj"  maxlength="120" placeholder="CPF ou CNPJ">
                </div>
                <div class="form-check">
                    <input class="form-check-input" type="checkbox" value="receveEmail" id="receveEmail" checked>
                    <label class="form-check-label" for="receveEmail">
                        Aceito receber comunicação via e-mail
                    </label>
                </div>
                <div class="form-check">
                    <input class="form-check-input" type="checkbox" value="receveSms" id="receveSms" checked>
                    <label class="form-check-label" for="receveSms">
                        Aceito receber comunicação via SMS
                    </label>
                </div>
            </div>
            <button class="btn btn-primary ml-lg-auto mr-lg-3 mx-auto mt-3">Entrar em contato</button>
        </div>
    </form>
</div>
