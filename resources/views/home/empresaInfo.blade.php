<div class="p-5">
    <h3 class="text-center">Conheça a concessionária</h3>
    <div class="d-flex flex-wrap">
        <div class="col-12 col-lg-4">
            <select id="lojaInfoSelect" name="lojaInfoSelect" class="custom-select">
                <option>Selecione uma loja</option>
                @foreach($lojas as $loja)
                    <option value="{{ $loja->loja_id }}">{{ $loja->name }}</option>
                @endforeach
            </select>
        </div>
        <div class="col-12 col-lg-8">
            @foreach($lojas as $key => $loja)
                <div class="lojaInfoDesc {{($key == 0)?"":"d-none"}}" data-loja="{{ $loja->loja_id }}">
                <div class="d-flex flex-wrap">
                    <div class="col-12 col-lg-6">
                        <h3><b>{{ $loja->name }}</b></h3>
                        <p>{{ utf8_encode($loja->street) }}, {{ utf8_encode($loja->door_number) }} - {{ utf8_encode($loja->neighborhood) }}
                            {{ utf8_encode($loja->city) }} - {{ utf8_encode($loja->state) }}</p>

                        <a href="{{ $loja->google_link_share }}" class="btn btn-primary col-12 col-lg-6" target="_blank"><i class="fas fa-map-marked-alt pr-2"></i>Como chegar</a>
                        <p><b>Geral</b></p>
                        @foreach($telefones->where('loja_id',1) as $phone)
                            <p>{{ $phone->phone }}</p>
                        @endforeach
                        <p><b>Geral</b></p>
                        <p>De segunda a sexta das 08:00 as 18:00</p>
                        <a href="{{ $loja->google_link_share }}">Mais informações sobre essa loja</a>
                    </div>
                    <div class="col-12 col-lg-6">
                        <?php echo $loja->google_map_embed;?>
                    </div>
                </div>
                </div>
            @endforeach
        </div>
    </div>
</div>
