@extends('layouts.layout')

@section('content')
    @include('home.carouselCars')
    @include('home.lineCars')
    @include('home.faleconosco')
    @include('home.servicos')
    @include('home.empresaInfo')
    @include('home.links')
@endsection
