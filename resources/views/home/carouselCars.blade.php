<div id="carouselCars" class="carousel slide" data-ride="carousel">
    <ol class="carousel-indicators">
        <li data-target="#carouselCars" data-slide-to="0" class="active"></li>
        <li data-target="#carouselCars" data-slide-to="1"></li>
    </ol>
    <div class="carousel-inner">
        <div class="carousel-item active">
            <img class="d-block w-100" src="images/sliders/xbanner_3805.jpeg.pagespeed.ic.6W73HlXRM1.jpg" alt="1">
        </div>
        <div class="carousel-item">
            <img class="d-block w-100" src="images/sliders/xbanner_5715.jpeg.pagespeed.ic.0o857q3u9W.jpg" alt="2">
        </div>
    </div>
    <a class="carousel-control-prev" href="#carouselCars" role="button" data-slide="prev">
        <span class="carousel-control-prev-icon" aria-hidden="true"></span>
        <span class="sr-only">Previous</span>
    </a>
    <a class="carousel-control-next" href="#carouselCars" role="button" data-slide="next">
        <span class="carousel-control-next-icon" aria-hidden="true"></span>
        <span class="sr-only">Next</span>
    </a>
</div>
