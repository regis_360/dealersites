<div class="bg-gray text-primary text-center p-5">
    <div class="d-flex flex-wrap">
        <div class="col-12 col-lg-3">
            <img src="/images/dealersites_logo.png" class="d-inline-block align-top" alt="">
            <p class="pt-4">Trabalhe Conosco</p>
            <div class="col-12">
                <i class="fab fa-facebook p-2"></i>
                <i class="fab fa-instagram p-2"></i>
                <i class="fab fa-youtube p-2"></i>
                <i class="fab fa-twitter p-2"></i>
            </div>
        </div>
        <div class="col-12 col-lg-9">
            <div class="d-flex flex-wrap">
            @foreach($menus as $menu)
                <div class="col-12 col-lg-4 pb-3">{{ utf8_encode($menu->name) }}</div>
            @endforeach
            </div>
        </div>
    </div>
</div>
