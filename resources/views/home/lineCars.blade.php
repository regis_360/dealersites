<div class="d-flex flex-wrap p-5">
    <h2 class="text-center w-100 text-primary">Linha Volkswagen</h2>
    <div class="d-flex flex-wrap">
        @foreach($carsModel as $car)
            <div class="col-6 col-lg-2">
                <img src="/images/cars/original_model_{{ $car }}.png " width="100%">
                <p class="text-center text-primary">Carro</p>
                <a href="#"><i class="fas fa-info-circle mr-2"></i>Mais detalhes desse modelo</a>
            </div>
        @endforeach
    </div>
</div>
