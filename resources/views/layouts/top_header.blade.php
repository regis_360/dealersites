<div class="bg-blue-dark col-12 d-flex p-2">
    <img src="/images/dealersites_logo.png" alt="">
    <div class="dropdown ml-auto px-2">
        <button class="btn btn-primary dropdown-toggle" type="button" id="loja" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
            <i class="fas fa-map-marker-alt"></i>
            @foreach($lojas as $key => $loja)
                <a class="loja-header-option {{($key == 0)?"":"d-none"}}" data-loja="{{$loja->loja_id}}" >{{ $loja->name }}</a>
            @endforeach
        </button>
        <div class="dropdown-menu dropdown-menu-right" aria-labelledby="loja">
            @foreach($lojas as $loja)
                <a class="dropdown-item loja-header" data-loja="{{$loja->loja_id}}">{{ $loja->name }}</a>
            @endforeach
        </div>
    </div>

    <div class="dropdown px-2">
        <button class="btn btn-primary dropdown-toggle" type="button" id="telefone" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
            <i class="fas fa-phone-alt"></i> Telefones
        </button>
        <div class="dropdown-menu dropdown-menu-right" aria-labelledby="telefone">
            @foreach($telefones as $telefone)
                <a class="dropdown-item loja-header-option {{($telefone->loja_id == $telefones[0]->loja_id)?"":"d-none"}}" href="#" data-loja="{{$telefone->loja_id}}">
                    @if($telefone->is_whatsapp)
                        <i class="fab fa-whatsapp"></i>
                    @else
                        <i class="fas fa-phone-alt"></i>
                    @endif
                    {{ $telefone->phone }} | {{ $telefone->label }}
                </a>
            @endforeach
        </div>
    </div>
</div>
