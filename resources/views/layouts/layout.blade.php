<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Document</title>
    <link rel="stylesheet" href="/css/layout.css">
    <link rel="stylesheet" href="/css/app.css">
</head>
<body>
    @include('layouts.top_header')
    @include('layouts.navbar')
    @yield('content')
    <script src="https://kit.fontawesome.com/a70abb4a98.js" crossorigin="anonymous"></script>
    <script src="js/jquery.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery.form/4.2.2/jquery.form.min.js" integrity="sha384-FzT3vTVGXqf7wRfy8k4BiyzvbNfeYjK+frTVqZeNDFl8woCbF0CYG6g2fMEFFo/i" crossorigin="anonymous"></script>
    <script src="js/bootstrap.js"></script>
    <script src="js/jquery.validate.js"></script>
    <script src="js/localization/messages_pt_BR.js"></script>
    <script src="js/jquery.mask.js"></script>
    <script src="js/contact-form.js"></script>
    <script src="js/app.js"></script>
</body>
</html>
