<nav class="navbar navbar-expand-lg navbar-light bg-primary text-white">
    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navContent" aria-controls="navContent" aria-expanded="false" aria-label="Toggle navigation">
        <span class="navbar-toggler-icon"></span>
    </button>

    <div class="collapse navbar-collapse" id="navContent">
        <ul class="navbar-nav w-100 justify-content-around">
            @foreach($menus as $menu)
                @if($menus->contains('parent_id',$menu->id))
                    <li class="nav-item dropdown">
                        <a class="nav-link dropdown-toggle text-white" href="#" id="{{ $menu->name }}Dropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                            {{ $menu->name }}
                        </a>
                        <div class="dropdown-menu dropdown-menu-right" aria-labelledby="{{ $menu->name }}Dropdown">
                            @foreach($menus->where('parent_id',$menu->id) as $submenu)
                                <a class="dropdown-item" href="#">{{ $submenu->name }}</a>
                            @endforeach
                        </div>
                    </li>
                @elseif(empty($menu->parent_id))
                    <li class="nav-item">
                        <a class="nav-link text-white" href="#">{{ $menu->name }}</a>
                    </li>
                @endif
            @endforeach
        </ul>
    </div>
</nav>
